package utils

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/true10/go-module/constants"
	"gitlab.com/true10/go-module/libraries"
)

type LineNotifyBody struct {
	Message LogBody `json:"message"`
}

type LogBody struct {
	LogType    string `json:"type"`
	PageName   string `json:"page_name"`
	ModuleName string `json:"module_name"`
	Message    string `json:"message"`
}

func LoggerInfo(pageName, moduleName string, message string) {
	body := LineNotifyBody{
		Message: LogBody{
			LogType:    "info",
			PageName:   pageName,
			ModuleName: moduleName,
			Message:    message,
		},
	}
	jsonBody, _ := json.Marshal(body)
	messageData := fmt.Sprintf("message=%s", string(jsonBody))
	callLineNofify(messageData)
}

func LoggerError(pageName, moduleName string, message string) {
	body := LineNotifyBody{
		Message: LogBody{
			LogType:    "error",
			PageName:   pageName,
			ModuleName: moduleName,
			Message:    message,
		},
	}
	jsonBody, _ := json.Marshal(body)
	messageData := fmt.Sprintf("message=❌ ❌ %s", string(jsonBody))
	callLineNofify(messageData)
}

func callLineNofify(message string) {
	var headers [][]string
	headers = append(headers, []string{"Authorization", "Bearer xYjvsqZGxhajX2zi9UjIpkD1MVtAqszoq1OyaNoC9RZ"})
	libraries.RequestHttp(http.MethodPost, "https://notify-api.line.me/api/notify", headers, constants.ApplicationForm, []byte(message), nil)
}
