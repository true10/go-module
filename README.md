How to create tag and use Go Module
1. After Push code to server
2. Find last version on repositories https://gitlab.com/true10/go-module/-/tags
3. Create Tag on go-module run " git tag -a v0.0.x -m "XXX" "
4. run "git push origin v0.0.x"
5. Go to microservice run "go get -u gitlab.com/true10/go-module@vx.x.x"
Ex. go get -u gitlab.com/true10/go-module@v0.0.1